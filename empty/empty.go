package main

import "fmt"

// type any = interface{} // type alias
// type any interface{}

func main() {
	var a any // interface{}

	a = 7
	fmt.Println(a)

	a = "hi"
	fmt.Println(a)

	s := a.(string) // type assertion
	fmt.Println("s:", s)

	// i := a.(int) // will panic
	if i, ok := a.(int); ok {
		fmt.Println("i:", i)
	} else {
		fmt.Println("not an int")
	}

}
