package tokenizer

import (
	"strings"
)

var (
	suffixes = []string{"ed", "ing", "s"}
)

// works, working, worked -> work
func Stem(word string) string {
	for _, s := range suffixes {
		if strings.HasSuffix(word, s) {
			return word[:len(word)-len(s)]
		}
	}

	return word
}

func initialSplit(text string) []string {
	//	var fs []string
	size := len(text) / 6 // By testing of 1000 samples from the corpus
	fs := make([]string, 0, size)
	i := 0
	for i < len(text) {
		// eat start
		for i < len(text) && !isLetter(text[i]) {
			i++
		}
		if i == len(text) {
			break
		}

		j := i + 1
		for j < len(text) && isLetter(text[j]) {
			j++
		}
		fs = append(fs, text[i:j])

		i = j
	}
	return fs

}

func isLetter(b byte) bool {
	return (b >= 'a' && b <= 'z') || (b >= 'A' && b <= 'Z')
}

// "Who's on first?" -> []string{"who", "on", "first"}
func Tokenize(text string) []string {
	text = strings.ToLower(text)
	var tokens []string
	for _, tok := range initialSplit(text) {
		//		tok = strings.ToLower(tok)
		tok = Stem(tok)
		if tok != "" {
			tokens = append(tokens, tok)
		}
	}
	return tokens
}
