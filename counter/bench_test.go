package main

import (
	"sync"
	"sync/atomic"
	"testing"
)

var mu sync.Mutex
var count int64

func incMutex() {
	mu.Lock()
	defer mu.Unlock()
	count++
}

func incAtomic() {
	atomic.AddInt64(&count, 1)
}

func BenchmarkMutex(b *testing.B) {
	for i := 0; i < b.N; i++ {
		incMutex()
	}
}

func BenchmarkAtomic(b *testing.B) {
	for i := 0; i < b.N; i++ {
		incAtomic()
	}
}
