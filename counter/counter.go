package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	// var mu sync.Mutex
	// count := 0
	var count int64

	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for j := 0; j < 1000; j++ {
				time.Sleep(time.Microsecond)
				/*
					mu.Lock()
					count++
					mu.Unlock()
				*/
				atomic.AddInt64(&count, 1)
			}
		}()
	}

	wg.Wait()
	fmt.Println(count)
}
