package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	// This bug is fixed in 1.22 (Feb 2024)
	for i := 0; i < 3; i++ {
		// Solution 2: Loop level variable
		i := i
		go func() {
			fmt.Println("goroutine:", i)
		}()
		/* Solution 1: Pass parameter
		go func(j int) {
			fmt.Println("goroutine:", j)
		}(i)
		*/

		/* BUG: All gorutines use the same "i" from line 12
		go func() {
			fmt.Println("goroutine:", i)
		}()
		*/
	}

	time.Sleep(time.Millisecond)

	// Synchronization: Access to shared resource
	// Coordination: Coordinate goroutines

	ch := make(chan string)
	go func() {
		ch <- "hi" // send
	}()
	msg := <-ch // receive
	fmt.Println(msg)

	fmt.Println(sleepSort([]int{20, 10, 30})) // [10 20 30]

	go func() {
		for i := 0; i < 4; i++ {
			msg := fmt.Sprintf("msg #%d", i)
			ch <- msg
		}
		close(ch)
	}()

	for msg := range ch {
		fmt.Println(msg)
	}
	val := <-ch // ch is closed
	fmt.Printf("closed: %q\n", val)
	val, ok := <-ch
	fmt.Printf("closed: %q (ok=%v)\n", val, ok)
	/* The for msg := range ch does:
	for {
		msg, ok := <- ch
		if !ok {
			break
		}
		// ...
	}
	*/
	// ch <- "hi" // ch is closed (panics)
	var ch2 chan int // ch2 is nil
	fmt.Println(ch2)

	ch1, ch2 := make(chan int), make(chan int)
	go func() {
		time.Sleep(10 * time.Millisecond)
		ch1 <- 1
	}()
	go func() {
		time.Sleep(20 * time.Millisecond)
		ch2 <- 2
	}()

	/*
		done := make(chan struct{})
		go func() {
			time.Sleep(time.Millisecond)
			close(done)
		}()
	*/
	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
	defer cancel()

	select {
	case v := <-ch1:
		fmt.Println("ch1:", v)
	case v := <-ch2:
		fmt.Println("ch2:", v)
	case <-time.After(5 * time.Millisecond):
		fmt.Println("timeout")
	//case <-done:
	case <-ctx.Done():
		fmt.Println("cancel")

		/*
			default:
				fmt.Println("no-one is ready")
		*/
	}

	/* Same bug without goroutines
	var fns []func()
	for i := 0; i < 3; i++ {
		fn := func() { fmt.Println("fn:", i) }
		fns = append(fns, fn)
	}
	for _, fn := range fns {
		fn()
	}
	*/
}

/* Channel semantics
- send/receive will block until opposite operation (guarantee of delivery) [*]
	- buffered channel had "n" sends without blocking
- receive from a closed channel will return the zero value without blocking
- send to a closed channel will panic (channel ownership)
- closing a closed channel will panic
- send/receive on a nil channel will block forever
*/

/*
	sleep sort algorithm:

- For every values "n" in values, spin a goroutine that will
  - sleep n milliseconds
  - send n over a channel

- collect all values from the channel and return them
*/
func sleepSort(values []int) []int {
	ch := make(chan int)
	for _, n := range values {
		n := n
		go func() {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()
	}

	var out []int
	// In 1.22: for range i {}
	for range values {
		val := <-ch
		out = append(out, val)
	}
	return out
}

func future[T any](fn func() T) chan T {
	ch := make(chan T, 1)
	go func() {
		ch <- fn()
	}()

	return ch
}

/* concurrency patterns
fan-out:
	Spread work to goroutines, then collect
async/future:
	Call now, get return later
producer/consumer:
	Shared channel as "queue"
fan-in:
	Gather several channels into single channel
dispatcher/load-balancer:
	Distribute work among several goroutines (single producer, multiple consumers)
*/
