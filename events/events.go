package main

import (
	"encoding/json"
	"fmt"

	"github.com/mitchellh/mapstructure"
)

var (
	msg1 = `{"type": "login", "user": "taz"}`
	msg2 = `{"type": "click", "x": 100, "y": 200}`
)

func main() {
	handleEvent([]byte(msg1))
	handleEvent([]byte(msg2))

}

/* Generics ?
type Array[T any] []T
var ints Array[int]
*/

// valid JSON < valid Data

// using mapstructure
func handleEvent(data []byte) error {
	var m map[string]any
	if err := json.Unmarshal(data, &m); err != nil {
		return err
	}
	fmt.Println(m)

	switch m["type"] {
	case "login":
		var e LoginEvent
		if err := mapstructure.Decode(m, &e); err != nil {
			return err
		}
		fmt.Printf("%#v\n", e) // TODO: Handle login
	case "click":
		var e ClickEvent
		if err := mapstructure.Decode(m, &e); err != nil {
			return err
		}
		fmt.Printf("%#v\n", e) // TODO: Handle click
	default:
		return fmt.Errorf("unknown event type: %#v", m["type"])
	}

	return nil
}

// working withing the standard library
func handleEvent1(data []byte) error {
	var m map[string]interface{}
	if err := json.Unmarshal(data, &m); err != nil {
		return err
	}
	fmt.Println(m)

	switch m["type"] {
	case "login":
		fmt.Println("login")
		login, ok := m["user"].(string)
		if !ok {
			return fmt.Errorf("user not a string: %#v (%T)", m["user"], m["user"])
		}
		e := LoginEvent{
			User: login,
		}
		fmt.Printf("%#v\n", e) // TODO: Handle login
	case "click":
		fmt.Println("click")
		e := ClickEvent{
			X: int(m["x"].(float64)), // might panic
			Y: int(m["y"].(float64)), // might panic
		}
		fmt.Printf("%#v\n", e) // TODO: Handle click
	default:
		return fmt.Errorf("unknown event type: %#v", m["type"])
	}
	return nil
}

type LoginEvent struct {
	User string
}

type ClickEvent struct {
	X int
	Y int
}
