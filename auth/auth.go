package main

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	/* Aside: Use #%v for debug/log
	a, b := 1, "1"
	fmt.Printf("a=%v, b=%v\n", a, b)
	fmt.Printf("a=%#v, b=%#v\n", a, b)
	*/

	if err := Login("daffy", "rabbitseason"); err != nil {
		fmt.Printf("error: %s\n", err)
		var ae *AuthError
		if errors.As(err, &ae) {
			fmt.Println("user:", ae.User)
		}
		return
	}
	fmt.Println("OK")

}

func Login(user, passwd string) error {
	// var err *AuthError // BUG: Will always be non-nil
	var err error

	/*
		return &AuthError{
			User:   user,
			Reason: "bad passwd",
		}
	*/

	// TODO: Check
	return err
}

func (e *AuthError) Error() string {
	return fmt.Sprintf("Auth: %q (%s)", e.User, e.Reason)
}

type AuthError struct {
	User   string
	Reason string
}

func OpenAny(uri string) (io.ReadCloser, error) {
	if strings.HasPrefix(uri, "file:///") {
		return os.Open(uri[7:])
	}

	// ...
	return nil, fmt.Errorf("don't know how to open %q", uri)
}
