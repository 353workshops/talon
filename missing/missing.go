package main

import (
	"encoding/json"
	"fmt"
)

type Job struct {
	User   string
	Action string
	Count  int
}

func DefaultJob() Job {
	return Job{
		Count: 1,
	}
}

func main() {
	data := []byte(`{
		"user": "saitama",
		"action": "punch"
	}`)

	job := DefaultJob()
	json.Unmarshal(data, &job)
	// TODO: Validate job
	fmt.Printf("%#v\n", job)

	// Option: Use pointers (Miki don't like it)
	// Option: Unmarshal to map[string]any (see mapstructure)
}
