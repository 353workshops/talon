/*
Always code as if the guy who ends up maintaining your code will be a violent
psychopath who knows where you live. Code for readability.
    - Martin Golding
*/

package main

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

func main() {
	banner("Go", 6)
	banner("G☻", 6)
	// byte = uint8
	// rune = int32
	s := "G☻!"
	fmt.Println("len:", len(s))    // in bytes
	fmt.Printf("s[3]: %c\n", s[3]) // offset in bytes
	for i, c := range s {          // runes
		fmt.Printf("%d: %c\n", i, c)
	}

	rs := []rune(s)
	fmt.Println(rs)

	s1, s2 := "Kraków", "Kraków"
	fmt.Println(s1 == s2)
	fmt.Println(len(s1), len(s2))
	// See Unicode normalization

	// Case insensitive: strings.EqualFold
}

func banner(text string, width int) {
	// offset := (width - len(text)) / 2
	offset := (width - utf8.RuneCountInString(text)) / 2
	fmt.Print(strings.Repeat(" ", offset))
	fmt.Println(text)
	fmt.Println(strings.Repeat("-", width))
}
