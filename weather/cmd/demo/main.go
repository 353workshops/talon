package main

import (
	"fmt"
	"log"

	"talon/weather/pb"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func main() {
	r := pb.Record{
		Time:    timestamppb.Now(),
		Station: "SFO",
		Temperature: &pb.Value{
			Amount: 8.1,
			Unit:   "c",
		},
		Rain: &pb.Value{
			Amount: 0,
			Unit:   "cm",
		},
	}
	fmt.Println(&r)

	data, err := proto.Marshal(&r)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Printf("size: %d\n", len(data))

	// jdata, err := json.Marshal(&r)
	jdata, err := protojson.Marshal(&r)
	if err != nil {
		log.Fatalf("error: (json) %s", err)
	}
	fmt.Printf("size: %d\n", len(jdata))
	fmt.Println(string(jdata))

}
