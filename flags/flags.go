package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
)

func main() {
	f := Write

	fmt.Println("f:", f)
	fmt.Printf(" s: %s\n", f)
	fmt.Printf(" v: %v\n", f)
	fmt.Printf("+v: %+v\n", f)
	fmt.Printf("#v: %#v\n", f)

	fmt.Println("JSON")
	json.NewEncoder(os.Stdout).Encode(f)
	// See json.Marshaler & json.Unmarshaler

	mask := Read | Write
	fmt.Println("mask:", mask) // read|write
}

// What printf does
func printf(v any) {
	if f, ok := v.(fmt.Stringer); ok {
		fmt.Println(f.String())
	}

	switch v.(type) { // type switch
	case int:
		// ...
	}

}

// For %#v implement GoStringer
// See also fmt.Formatter
// go install golang.org/x/tools/cmd/stringer@latest
// export PATH="$(go env GOPATH)/bin:${PATH}"
// Exercise (HW): Read the output of "stringer -type=Flag"

/* Another option
var names map[Flag]string{
	Read: "read",
}
*/

// Exercise: Support bitmask
// e.g. Read|Write -> "read|write"
// String implements fmt.Stringer
func (f Flag) String() string {
	switch f {
	case Read:
		// fallthrough
		return "read"
	case Write:
		return "write"
	case Execute:
		return "execute"
	}

	if f < Read || f >= maxFlag {
		return fmt.Sprintf("<Flag %d>", f)
	}

	var buf strings.Builder
	i := 0
	for v := Read; v <= Execute; v <<= 1 {
		if f&v != 0 {
			if i > 0 {
				buf.WriteByte('|')
			}
			buf.WriteString(v.String())
			i++
		}
	}

	return buf.String()
}

const (
	Read Flag = 1 << iota // bitmask
	//Read Flag = 1 + iota
	Write
	Execute
	maxFlag
)

type Flag byte
