package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	v := Value{12.3, Celsius}
	data, err := json.Marshal(v)
	if err != nil {
		fmt.Println("error: marshal", err)
		return
	}
	fmt.Println(string(data))

	var v2 Value
	if err := json.Unmarshal(data, &v2); err != nil {
		fmt.Println("error: unmarshal", err)
	}
	fmt.Printf("%+v\n", v2)
}

func (v *Value) UnmarshalJSON(data []byte) error {
	var a float64
	var u Unit

	s := string(data[1 : len(data)-1]) // drop ""
	if _, err := fmt.Sscanf(s, "%f%s", &a, &u); err != nil {
		return err
	}

	jv := Value{a, u}
	if err := jv.Validate(); err != nil {
		return err
	}

	//*v = jv
	v.Amount = jv.Amount
	v.Unit = jv.Unit
	return nil
}

func (v Value) Validate() error {
	switch v.Unit {
	case Celsius, Fahrenheit, Kelvin:
		// NOP
	default:
		return fmt.Errorf("%q - bad unit", v.Unit)
	}

	// TODO: Kelvin < 0 ...

	return nil
}

// MarshalJSON implements json.Marshaler
func (v Value) MarshalJSON() ([]byte, error) {
	// Step 1: Convert to type that encoding/json knows
	s := fmt.Sprintf("%f%s", v.Amount, v.Unit)
	// Step 2: Use json.Marshal
	return json.Marshal(s)
	// Step 3: There is no step 3

}

type Unit string

const (
	Celsius    Unit = "c"
	Fahrenheit Unit = "f"
	Kelvin     Unit = "k"
)

type Value struct {
	Amount float64 `json:"amount,omitempty"` // field tag
	Unit   Unit    `json:"unit"`
}

/* encoding/json API
JSON -> []byte -> Go: Unmarshal
JSON -> io.Reader -> Go: json.NewDecoder
Go -> []byte -> JSON: Marshal
Go -> io.Writer -> JSON: json.NewEncoder

Will fill only exported fields.
Unknown fields are ignored (both sides).
*/

/* JSON <-> Go
object <-> map[string]any, struct
array <-> []T ([]any)
string <-> string
number <-> [float64], float32, int, int8, int16, int32, int64, uint, uint8 ... uint64
bool <-> bool
null <-> nil

time.Time -> string
[]byte -> base64 string
*/
