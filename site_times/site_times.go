package main

import (
	"context"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"sync"
	"time"
)

func main() {
	urls := []string{
		"https://www.google.com",
		"https://www.apple.com",
		"https://httpbin.org/status/404",
	}

	siteTimes(context.Background(), urls)
}

func siteTimes(ctx context.Context, urls []string) {
	var wg sync.WaitGroup

	wg.Add(len(urls))
	for _, url := range urls {
		// wg.Add(1)
		url := url
		go func() {
			defer wg.Done()
			siteTime(ctx, url)
		}()
	}

	wg.Wait()
}

func siteTime(ctx context.Context, url string) (time.Duration, error) {
	start := time.Now()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return 0, err
	}

	// resp, err := http.Get(url)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return 0, err
	}

	if resp.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("bad status: %d %s", resp.StatusCode, resp.Status)
	}

	_, err = io.Copy(io.Discard, resp.Body)
	if err != nil {
		return 0, err
	}

	duration := time.Since(start)
	slog.Info("site time", "url", url, "duration", duration)
	return 0, nil
}
