# Go Workshop @ Talon

Miki Tebeka
📬 [miki@353solutions.com.com](mailto:miki@353solutions.com), 𝕏 [@tebeka](https://twitter.com/tebeka), 👨 [mikitebeka](https://www.linkedin.com/in/mikitebeka/), ✒️[blog](https://www.ardanlabs.com/blog/)

#### Shameless Plugs

- [LinkedIn Learning Classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Books](https://pragprog.com/search/?q=miki+tebeka)

[Syllabus](_extra/syllabus.pdf)  
[Final Exercise](_extra/dld.md)

---

## Day 1 - Interfaces in Depth

### Agenda

- Understanding Go history & use cases
- Working with interfaces
- Interfaces in the standard library
- Using generics to reduce code size

### Code

- [stack.go](stack/stack.go) - Generic data structure
- [deep.go](deep/deep.go) - Using generics
- [flags.go](flags/flags.go) - Implementing `fmt.Stringer`
- [client.go](client/client.go) - Mocking `http.RoundTripper`
- [empty.go](empty/empty.go) - The empty interface (`any`)
- [auth.go](auth/auth.go) - Custom errors and nil/non-nil interfaces
- [logger.go](logger/logger.go) - Keep interfaces small
- [sha1.go](sha1/sha1.go) - Combining `io.Reader`

### Exercise

- [Painting Program](_extra/draw.md)
- Read and understand and [sort package examples](https://pkg.go.dev/sort#pkg-examples)

### Links

- [Generics can make your Go code slower](https://planetscale.com/blog/generics-can-make-your-go-code-slower)
- [Memory Models](https://research.swtch.com/mm) by Russ Cox
- [pkg/errors](https://github.com/pkg/errors)
- [Go proverbs](https://go-proverbs.github.io/)
- [sort examples](https://pkg.go.dev/sort/#pkg-examples) - Read and try to understand
- [When to use generics](https://go.dev/blog/when-generics)
- [Generics tutorial](https://go.dev/doc/tutorial/generics)
- [Methods, interfaces & embedded types in Go](https://www.ardanlabs.com/blog/2014/05/methods-interfaces-and-embedded-types.html)
- [Methods & Interfaces](https://go.dev/tour/methods/1) in the Go tour
- [encoding/json](https://pkg.go.dev/encoding/json)
- [net/http](https://pkg.go.dev/net/http)
- [stringer](https://pkg.go.dev/golang.org/x/tools/cmd/stringer) tool

### Data & Other

- [http.log.gz](_extra/http.log.gz)
- [road.txt](_extra/road.txt)

### Rules of thumb

- Accept interfaces, return types
- Start with types, discover interfaces
- Interface say what we need (not what we provide)
- Interfaces should be small (<= 5 methods)
- Use generics for type safe container types or remove code duplication

---

## Day 2 - Concurrency

### Agenda

- How Go’s runtime handles concurrency
- Using channels and their semantics
- Using context.Context for timeouts & cancellations
- A look at the “sync” and “sync/atomic” packages

### Code


- [counter.go](counter/counter.go) - Using the race detector
    - [bench_test.go](counter/bench_test.go) - Benchmarking `sync.Mutex` vs `atomic`
- [site_times.go](site_times/site_times.go) - Using `sync.WaitGroup` to wait for goroutines
- [panic.go](panic/panic.go) - Panics in goroutines to crash a server
- [writer.go](writer/writer.go) - Throttling writes using buffered channels & select
- [rtb.go](rtb/rtb.go) - Using context
- [go_chan.go](go_chan/go_chan.go) - goroutines & channels

### Exercise

In [taxi.go](taxi/taxi.go), create:
```go
func CheckDirectory(ctx context.Context, rootDir string, n int) error {
}
```

It should check the validity of files as we did in class, but limit the runtime by `ctx` and use only `n` goroutines at a time.

### Links

- [tar](https://xkcd.com/1168/) ☺
- [An Introduction to go tool trace](https://about.sourcegraph.com/blog/go/an-introduction-to-go-tool-trace-rhys-hiltner)
- [idempotence](https://en.wikipedia.org/wiki/Idempotence)
- [automaxprocs](https://pkg.go.dev/go.uber.org/automaxprocs) - From Uber, set `GOMAXPROCS` in k8s and friends
- [HACKING](https://go.dev/src/runtime/HACKING) in the Go runtime
- [Ardan Labs services](https://github.com/ardanlabs/service)
- [The race detector](https://go.dev/doc/articles/race_detector)
- [Uber Go Style Guide](https://github.com/uber-go/guide/blob/master/style.md)
- [errgroup](https://pkg.go.dev/golang.org/x/sync/errgroup)
- [Context](https://blog.golang.org/context) on the Go blog
- [Data Race Patterns in Go](https://eng.uber.com/data-race-patterns-in-go/)
- [Go Concurrency Patterns: Pipelines and cancellation](https://go.dev/blog/pipelines)
- [Go Concurrency Patterns: Context](https://go.dev/blog/context)
- [Curious Channels](https://dave.cheney.net/2013/04/30/curious-channels)
- [The Behavior of Channels](https://www.ardanlabs.com/blog/2017/10/the-behavior-of-channels.html)
- [Channel Semantics](https://www.353solutions.com/channel-semantics)
- [Why are there nil channels in Go?](https://medium.com/justforfunc/why-are-there-nil-channels-in-go-9877cc0b2308)
- [Amdahl's Law](https://en.wikipedia.org/wiki/Amdahl%27s_law) - Limits of concurrency
- [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
- [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso) by Rob Pike
- [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part2.html) by Bill Kennedy

### Data & Other

- [taxi.tar](https://storage.googleapis.com/353solutions/c/data/taxi.tar)

---

## Day 3 - Serialization, Going Faster & More Secure

### Agenda

- Effective serialization
    - JSON serialization
    - A look at Protocol buffers
- Writing Secure Go Code
    - Patching an insecure app
    - OWASP Top 10
    - Security tools
    - Metrics, logging and process
- A look at performance optimization
    - Why you shouldn't optimize
    - CPU optimization: Measuring code speed, using the profiler
    - Memory optimization: Measuring memory consumption, using the profiler
    - General rules and guidelines


### Code

TBD

### Links

- Serialization
    - Unicode
        - [Plain Text](https://www.youtube.com/watch?v=4mRxIgu9R70)
        - [golang.org/x/text](https://pkg.go.dev/golang.org/x/text?tab=doc)
        - [Unicode Table](https://unicode-table.com/en/)
        - [Arrays, slices & strings](https://blog.golang.org/slices)
        - [Strings, bytes, runes and characters in Go](https://blog.golang.org/strings)
    - JSON
        - [encoding/json](https://golang.org/pkg/encoding/json/) - Use this
        - [Custom marshalling](https://golang.org/pkg/encoding/json/#example__customMarshalJSON)
        - [Using anonymous nested structs](https://pythonwise.blogspot.com/2020/01/nested-structs-for-http-calls.html)
        - [mapstructure](https://github.com/mitchellh/mapstructure) - Decode `map[string]any` to a struct
        - [Chunked HTTP in Go](https://www.alexedwards.net/blog/how-to-use-the-http-responsecontroller-type)
        - [jsonlines](https://jsonlines.org/examples/)
    - Protocol Buffers
        - [Protocol Buffer Basics: Go](https://developers.google.com/protocol-buffers/docs/gotutorial)
        - [google.golang.org/protobuf/proto](https://pkg.go.dev/google.golang.org/protobuf/proto?tab=doc)
        - [A new Go API for Protocol Buffers](https://blog.golang.org/protobuf-apiv2)
        - [protoc Download](https://github.com/protocolbuffers/protobuf/releases/)
        - [gRPC](https://grpc.io/) - Protocol Buffers over HTTP2
        - [Buf](https://buf.build/) - Protocol Buffers tools
        - [protojson](https://pkg.go.dev/google.golang.org/protobuf/encoding/protojson) Marshal protobuf generate objects to JSON the right way (don't use `encoding/json` on them)
    - [Gobs of data](https://blog.golang.org/gob) in the Go blog
- Security
    - [Go Security Policy](https://golang.org/security)
    - [Awesome security tools](https://github.com/guardrailsio/awesome-golang-security)
    - [How our security team handles secrets](https://monzo.com/blog/2019/10/11/how-our-security-team-handle-secrets)
    - [Using Let's Encrypt in Go](https://marcofranssen.nl/build-a-go-webserver-on-http-2-using-letsencrypt)
    - Books
        - [Security with Go](https://www.packtpub.com/product/security-with-go/9781788627917)
        - [Black Hat Go](https://nostarch.com/blackhatgo) book
    - [Search for AWS keys in GitHub](https://sourcegraph.com/search?q=context:global+AWS_SECRET_ACCESS_KEY%3D%5B%27%22%5D.%7B40%7D%5B%27%22%5D&patternType=regexp)
    - [Fallacies of distributed computing](https://en.wikipedia.org/wiki/Fallacies_of_distributed_computing#The_fallacies)
    - [cue](https://cuelang.org/) - Language for data validation
    - Serialization Vulnerabilities
        - [XML Billion Laughs](https://en.wikipedia.org/wiki/Billion_laughs_attack) attack
        - [Java Parse Float](https://www.exploringbinary.com/java-hangs-when-converting-2-2250738585072012e-308/)
    - [Resilient net/http servers](https://ieftimov.com/post/make-resilient-golang-net-http-servers-using-timeouts-deadlines-context-cancellation/)
    - [Our Software Depenedcy Problem](https://research.swtch.com/deps)
    - Static tools
        - [gosec](https://github.com/securego/gosec)
        - `go install golang.org/x/vuln/cmd/govulncheck@latest`
        - [staticcheck](https://staticcheck.io/)
    - [OWASP Top 10](https://owasp.org/www-project-top-ten/)
    - [The Security Mindset](https://www.schneier.com/blog/archives/2008/03/the_security_mi_1.html) by Bruce Schneier
- Performance
    - [Rules of Optimization Club](https://wiki.c2.com/?RulesOfOptimizationClub)
    - [benchstat](https://pkg.go.dev/golang.org/x/perf/cmd/benchstat)
    - [Benchmarking Checklist](https://www.brendangregg.com/blog/2018-06-30/benchmarking-checklist.html)
    - [Rob Pike's 5 Rules of Programming](https://users.ece.utexas.edu/~adnan/pike.html)
    - [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576)
    - [A Guide to the Go Garbage Collector](https://go.dev/doc/gc-guide)
    - [Garbage Collection In Go](https://www.ardanlabs.com/blog/2018/12/garbage-collection-in-go-part1-semantics.html)
    - [Getting Friendly with CPU Caches](https://www.ardanlabs.com/blog/2023/07/getting-friendly-with-cpu-caches.html)
    - [Language Mechanics](https://www.ardanlabs.com/blog/2017/05/language-mechanics-on-stacks-and-pointers.html)
    - [Tracing Programs with Go](https://www.youtube.com/watch?v=mjixFKO-IdM)
    - [High Performance Go Workshop](https://dave.cheney.net/high-performance-go-workshop/gophercon-2019.html)
    - [High Performance Networking in Chrome](https://aosabook.org/en/posa/high-performance-networking-in-chrome.html)
    - [graphviz](https://graphviz.org/)
    - [Profile-guided optimization](https://go.dev/doc/pgo)

### Data & Other

- [CPU cache](_extra/cpu-cache.png)
- [Complexity](_extra/complexity.png)
- [Stack](_extra/stack.png)
- [stop_words.txt](_extra/stop_words.txt)
- `curl https://api.stocktwits.com/api/2/streams/symbol/PANW.json`
