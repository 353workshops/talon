package client

import (
	"fmt"
	"net/http"
	"testing"
)

type mockTripper struct{}

func (mockTripper) RoundTrip(*http.Request) (*http.Response, error) {
	return nil, fmt.Errorf("no connection")

}

func TestHealthError(t *testing.T) {
	c, err := New("https://api.example.com")
	if err != nil {
		t.Fatal(err)
	}

	// Mock
	c.c.Transport = mockTripper{}

	err = c.Health()
	if err == nil {
		t.Fatal(err)
	}
}
