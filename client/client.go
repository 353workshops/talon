package client

import (
	"fmt"
	"net/http"
)

type Client struct {
	c       http.Client
	baseURL string
}

func New(baseURL string) (*Client, error) {
	if baseURL == "" {
		return nil, fmt.Errorf("bad client")
	}

	c := Client{
		baseURL: baseURL,
	}

	// Readability tip: Add the & at the return statement
	return &c, nil
}

func (c *Client) Health() error {
	url := fmt.Sprintf("%s/health", c.baseURL)
	resp, err := c.c.Get(url)

	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf(resp.Status)
	}

	return nil
}
