package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	l := New(os.Stdout)
	l.Info("The world is flat! (not)")
}

type syncer interface {
	Sync() error
}

func (l *Logger) Info(msg string) {
	fmt.Fprintf(l.w, "INFO: %s\n", msg)
	l.s.Sync()
}

// Check that type implements interface
// Usually anti-pattern
// var _ syncer = nopSyncer{}

type nopSyncer struct{}

func (nopSyncer) Sync() error {
	return nil
}

func New(w io.Writer) *Logger {
	l := Logger{w: w, s: nopSyncer{}}
	if s, ok := l.w.(syncer); ok {
		l.s = s
	}
	return &l
}

type Logger struct {
	w io.Writer
	s syncer
}

/*
type Syncer interface {
	io.Writer
	Sync() error
}
*/

/* Thought experiment: sort interface
type Sortable interface {
	Less(i, j int) bool
	Swap(i, j int)
	Len() int
}

func sort(s Sortable) {
	// ...
}

*/
