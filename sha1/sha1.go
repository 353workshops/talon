package main

import (
	"compress/gzip"
	"crypto/sha1"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

/* Go
type Reader interface {
	Read(p []byte) (n int, err error)
}

Python
type Reader interface {
	Read(n int) (p []byte, err error)
}
*/

func main() {
	// GoLand "sha1/http.log.gz"
	fmt.Println(fileSha1("http.log.gz"))
	fmt.Println(fileSha1("sha1.go"))

	if _, err := fileSha1("/tmp/a.gz"); err != nil {
		for e := err; e != nil; e = errors.Unwrap(e) {
			fmt.Println(">>>", e)
		}
	}
}

// Exercise: Decompress only if file name ends with .gz
// Use sha1.go to check
// cat http.log.gz | gunzip | sha1sum
// cat sha1.go | sha1sum
func fileSha1(fileName string) (string, error) {
	// Idiom: accept interfaces, return types
	// cat http.log.gz
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	var r io.Reader = file

	if strings.HasSuffix(fileName, ".gz") {
		// | gunzip
		var err error
		r, err = gzip.NewReader(file)
		if err != nil {
			return "", fmt.Errorf("%q: not a gzip file? - %w", fileName, err)
		}
	}

	// | sha1sum
	w := sha1.New()
	if _, err := io.Copy(w, r); err != nil {
		return "", fmt.Errorf("%q: can't compute SHA1 - %w", fileName, err)
	}

	return fmt.Sprintf("%x", w.Sum(nil)), nil
}
