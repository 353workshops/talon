package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan string, 1024)
	go producer(ch)
	go worker(ch)

	select {}
}

func producer(ch chan<- string) {
	i := 0
	for {
		time.Sleep(5 * time.Millisecond)
		i++
		msg := fmt.Sprintf("msg #%d", i)
		select {
		case ch <- msg:
			// NOP
		default:
			fmt.Printf("dropping %s\n", msg)
		}
	}

}

// consumer
func worker(ch <-chan string) {
	for msg := range ch {
		fmt.Println("got:", msg)
		time.Sleep(10 * time.Millisecond)

	}
}
