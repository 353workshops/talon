#!/bin/bash

set -e

case "$1" in
    -h | --help ) echo "usage: $(basename $0) FILE"; exit;;
esac


if [ $# -ne 1 ]; then
    2>&1 echo "error: bad number of arguments"
    exit 1
fi

if [ ! -f "$1" ]; then
    2>&1 echo "error: $1 - no such file"
    exit 1
fi

curl -f -d@"$1" http://localhost:8080/new
