package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

const (
	urlTemplate = "https://api.stocktwits.com/api/2/streams/symbol/%s.json"
)

func relatedStocks(symbol string) (map[string]int, error) {
	url := fmt.Sprintf(urlTemplate, url.PathEscape(symbol))
	r, err := http.Get(url) // TODO: context
	if err != nil {
		return nil, err
	}

	defer r.Body.Close()
	if r.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%q: bad status - %s", symbol, r.Status)
	}

	var reply struct { // anonymous struct
		Messages []struct {
			Symbols []struct {
				Name string `json:"symbol"`
			}
		}
	}

	if err := json.NewDecoder(r.Body).Decode(&reply); err != nil {
		return nil, err
	}

	counts := make(map[string]int)
	for _, m := range reply.Messages {
		for _, s := range m.Symbols {
			if !strings.EqualFold(s.Name, symbol) {
				counts[s.Name]++
			}
		}
	}

	return counts, nil
}

func main() {
	/*
		if len(os.Args) != 2 {
			// FIXME: use the flag package
			log.Fatal("error: wrong number of arguments")
		}

		symbol := os.Args[1]
		counts, err := relatedStocks(symbol)
		if err != nil {
			log.Fatal(err)
		}
	*/
	counts, err := relatedStocks("PANW")
	if err != nil {
		log.Printf("error: %s", err)
		os.Exit(1)
	}

	for sym, n := range counts {
		fmt.Printf("%10s %d\n", sym, n)
	}
}
