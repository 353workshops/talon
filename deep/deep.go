package deep

import "fmt"

type OrderedNum interface {
	~int | ~float64

	// Kind() string
}

type Ordered interface {
	OrderedNum | ~string
}

func zero[T any]() (out T) {
	return
}

// Exercise: Write Max & test it
func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		/*
			var zero T
			return zero, fmt.Errorf("Max of empty slice")
		*/
		return zero[T](), fmt.Errorf("Max of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}
	return m, nil
}

func Relu[T OrderedNum](n T) T {
	if n < 0 {
		return 0
	}

	return n
}

/*
func ReluInt(n int) int {
	if n < 0 {
		return 0
	}

	return n
}

func ReluFloat64(n float64) float64 {
	if n < 0 {
		return 0
	}

	return n
}

*/
